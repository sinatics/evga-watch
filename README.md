# EVGA Watch

## What does it do?
This container checks for a handful of 3080 listings on EVGA's website and sends a push notification to your phone via pushover on a 30 second loop.  

All that is required to get this up and running is a couple api tokens from pushover and building the docker container which is outlined below.  

Code can be modified to watch more url's or remove urls for cards that you don't care about.  

## How do I use it?
This code runs in a docker container and requires 2 environment variables.
See the `Setting up your Dev Environment` area below if you'd like to contribute to the project or run this code locally with dev params.  

Code lives under:  
`fs-overlay/opt/evga-watch/lib/evga.py`

## Run Params for evga-watch
The following envorionment variables must be passed to the docker container or it will exit:  
`PUSHOVER_APP_TOKEN`  
`PUSHOVER_USER_KEY`  
`SLACK_API_TOKEN`  

If you only want to use Slack or Pushover, you can use fake credentials for the service you aren't using. They still need to be included though because I'm lazy.

## Example Docker Build & Run command

To Build for deployment:
```
docker build --tag=evga-watch .
```

To Run image from build:
```
docker run -it -d \
    --name=evga-watch \
    -e PUSHOVER_APP_TOKEN='YOUR_TOKEN' \
    -e PUSHOVER_USER_KEY='YOUR_KEY' \
    -e SLACK_API_TOKEN='xoxb-xxxxxxxxx' \
    evga-watch
```

# Setting up your Dev Environment
The evga-watch codebase is setup in a way that you can run dev work from the production ready docker container. The following steps will get evga-watch running on your system in no time.
1. After cloning this repo, find the `example-dev-config.eyaml` file and rename it to `dev-config.eyaml`, after that put your api tokens from pushover into that file.
2. Run the dev docker container with `./dev_run.sh`
3. I'd encourage you to look over the content of dev_run.sh, but essentially each time it's run it does the following:  
1.1 Parses secrets down from `dev-config.eyaml` as environment variables of the format config_developer_<secret name>.  
1.2 Rebuilds the Dockerfile and tags it as `evga-watch-dev`.  
1.3 Runs `docker stop evga-watch-dev` and `docker rm evga-watch-dev` to remove any running/old containers.  
1.4 Runs `docker run`, naming the container `evga-watch-dev` and importing all required secrets from the parsed eyaml environment variables from step 1.1  
1.5 Tails the container logs with `docker logs -f evga-watch-dev`  
4. You've now got a fully setup dev environment for evga-watch. Start developing by modfying code under `fs-overlay/opt/evga-watch/lib`!
