import requests
import re
import sys
import os
import time
import datetime

# Only proceed if required environment variables are set
required_vars = ['PUSHOVER_APP_TOKEN', 'PUSHOVER_USER_KEY', 'SLACK_API_TOKEN']

for required_var in required_vars:
    if required_var not in os.environ:
        print(f'{required_var} Wasn\'t set, This is a required variable. Exiting')
        sys.exit()

if 'DEV' not in os.environ:
    DEV = False
else:
    DEV = True

PUSHOVER_APP_TOKEN = os.environ['PUSHOVER_APP_TOKEN']
PUSHOVER_USER_KEY = os.environ['PUSHOVER_USER_KEY']
SLACK_API_TOKEN = os.environ['SLACK_API_TOKEN']


def send_slack_msg(msg):

    try:
        response = requests.post(
            url="https://slack.com/api/chat.postMessage",
            params={
                "token": SLACK_API_TOKEN,
                "icon_emoji": "':partyparrot:'",
                "username": "GetDat3080",
                "channel": "#3080plz",
                "text": msg,
            },
        )
        print('Response HTTP Status Code: {status_code}'.format(
            status_code=response.status_code))
        print('Response HTTP Response Body: {content}'.format(
            content=response.content))
    except requests.exceptions.RequestException:
        print('HTTP Request failed')


def pushover_msg(msg):

    url = 'https://api.pushover.net/1/messages.json'

    headers = {
        "Content-type": "application/x-www-form-urlencoded"
    }

    data = {
        "token": PUSHOVER_APP_TOKEN,
        "user": PUSHOVER_USER_KEY,
        "message": msg,
    }

    r = requests.post(url, headers=headers, data=data)

    print(r.json())


def get_current_datetime():
    return datetime.datetime.now()


def datetime_to_str(datetime_obj):
    return datetime_obj.strftime("%Y-%m-%d %H:%M:%S")


def str_to_datetime(datetime_str):
    return datetime.datetime.strptime(datetime_str, '%Y-%m-%d %H:%M:%S')


def get_page_html(url):

    headers = {
        'User-Agent': "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_6) AppleWebKit/537.36 (KHTML, like Gecko) "
                      "Chrome/85.0.4183.102 Safari/537.36"
    }

    r = requests.get(url, headers=headers)
    return r.text


def evga_stock(url):
    html_text = get_page_html(url)
    match = re.search(r'\bADD TO CART\b', str(html_text))
    if match:  # Return true if In Stock
        return True
    else:  # Return false if Out of Stock
        return False


def check_evga_stock_slack_service():

    cards_to_check_dev = [
        {'card_name': 'TEST CARD - EVGA GeForce GTX 1660 XC BLACK GAMING', 'price': '$229.99', 'url':'https://www.evga.com/products/product.aspx?pn=06G-P4-1161-KR', 'last_alert': '2020-09-20 20:02:28'}
    ]

    cards_to_check = [
        {'card_name': 'EVGA GeForce RTX 3080 FTW3 ULTRA GAMING', 'price': '$809.99', 'url': 'https://www.evga.com/products/product.aspx?pn=10G-P5-3897-KR', 'last_alert': '2020-09-20 20:02:28'},
        {'card_name': 'EVGA GeForce RTX 3080 XC3 BLACK GAMING', 'price': '$699.99','url': 'https://www.evga.com/products/product.aspx?pn=10G-P5-3881-KR', 'last_alert': '2020-09-20 20:02:28'},
        {'card_name': 'EVGA GeForce RTX 3080 XC3 GAMING', 'price': '$749.99', 'url': 'https://www.evga.com/products/product.aspx?pn=10G-P5-3883-KR', 'last_alert': '2020-09-20 20:02:28'},
        {'card_name': 'EVGA GeForce RTX 3080 XC3 ULTRA GAMING', 'price': '$769.99', 'url': 'https://www.evga.com/products/product.aspx?pn=10G-P5-3885-KR', 'last_alert': '2020-09-20 20:02:28'}
    ]

    # Keep a daemon running forever
    while True:
        if DEV:
            for card in cards_to_check_dev:
                if evga_stock(card['url']):
                    if (get_current_datetime() - str_to_datetime(card['last_alert'])).seconds > 1800:  # Only alert if we haven't in the past 30 minutes
                        card['last_alert'] = datetime_to_str(get_current_datetime())  # Update last_alert to current datetime.
                        print(f'DEV IN STOCK - {card}')
        else:
            for card in cards_to_check:
                if evga_stock(card['url']):
                    if (get_current_datetime() - str_to_datetime(card['last_alert'])).seconds > 1800:  # Only alert if we haven't in the past 30 minutes
                        card['last_alert'] = datetime_to_str(get_current_datetime())  # Update last_alert to current datetime.
                        send_slack_msg(f"<!here> IN STOCK - "
                                       f"{card['card_name']} - "
                                       f"{card['price']}\n"
                                       f"{card['url']}")

        time.sleep(30)


def check_evga_stock_pushover_service():

    cards_to_check_dev = [
        {'card_name': 'TEST CARD - EVGA GeForce GTX 1660 XC BLACK GAMING', 'price': '$229.99', 'url':'https://www.evga.com/products/product.aspx?pn=06G-P4-1161-KR', 'last_alert': '2020-09-20 20:02:28'}
    ]

    cards_to_check = [
        {'card_name': 'EVGA GeForce RTX 3080 FTW3 ULTRA GAMING', 'price': '$809.99', 'url': 'https://www.evga.com/products/product.aspx?pn=10G-P5-3897-KR', 'last_alert': '2020-09-20 20:02:28'},
        {'card_name': 'EVGA GeForce RTX 3080 XC3 BLACK GAMING', 'price': '$699.99','url': 'https://www.evga.com/products/product.aspx?pn=10G-P5-3881-KR', 'last_alert': '2020-09-20 20:02:28'},
        {'card_name': 'EVGA GeForce RTX 3080 XC3 GAMING', 'price': '$749.99', 'url': 'https://www.evga.com/products/product.aspx?pn=10G-P5-3883-KR', 'last_alert': '2020-09-20 20:02:28'},
        {'card_name': 'EVGA GeForce RTX 3080 XC3 ULTRA GAMING', 'price': '$769.99', 'url': 'https://www.evga.com/products/product.aspx?pn=10G-P5-3885-KR', 'last_alert': '2020-09-20 20:02:28'}
    ]

    # Keep a daemon running forever
    while True:
        if DEV:
            for card in cards_to_check_dev:
                if evga_stock(card['url']):
                    if (get_current_datetime() - str_to_datetime(card['last_alert'])).seconds > 1800:  # Only alert if we haven't in the past 30 minutes
                        card['last_alert'] = datetime_to_str(get_current_datetime())  # Update last_alert to current datetime.
                        print(f'DEV IN STOCK - {card}')
        else:
            for card in cards_to_check:
                if evga_stock(card['url']):
                    if (get_current_datetime() - str_to_datetime(card['last_alert'])).seconds > 1800:  # Only alert if we haven't in the past 30 minutes
                        card['last_alert'] = datetime_to_str(get_current_datetime())  # Update last_alert to current datetime.
                        pushover_msg(f"IN STOCK:\n"
                                       f"{card['card_name']}\n"
                                       f"{card['price']}\n"
                                       f"{card['url']}")

        time.sleep(30)


if __name__ == '__main__':
    check_evga_stock_slack_service()
