#!/bin/bash

# eyaml parsing function and reading in secrets
parse_yaml() {
   local prefix=$2
   local s='[[:space:]]*' w='[a-zA-Z0-9_]*' fs=$(echo @|tr @ '\034')
   sed -ne "s|^\($s\)\($w\)$s:$s\"\(.*\)\"$s\$|\1$fs\2$fs\3|p" \
        -e "s|^\($s\)\($w\)$s:$s\(.*\)$s\$|\1$fs\2$fs\3|p"  $1 |
   awk -F$fs '{
      indent = length($1)/2;
      vname[indent] = $2;
      for (i in vname) {if (i > indent) {delete vname[i]}}
      if (length($3) > 0) {
         vn=""; for (i=0; i<indent; i++) {vn=(vn)(vname[i])("_")}
         printf("%s%s%s=\"%s\"\n", "'$prefix'",vn, $2, $3);
      }
   }'
}

# read yaml file, values are set as environment variables under the structure "config_..."
eval $(parse_yaml dev-config.eyaml "config_")

project_name='evga-watch'

echo "Rebuilding ${project_name}-dev Container"
docker build --tag=${project_name}-dev .

echo "Restarting ${project_name}-dev Container"
docker stop ${project_name}-dev || true && docker rm ${project_name}-dev || true

# Customize run command as needed to include environment variables, ports and any other docker flags.
docker run -it -d \
    --name=${project_name}-dev \
    -e PUSHOVER_APP_TOKEN=${config_development_PUSHOVER_APP_TOKEN} \
    -e PUSHOVER_USER_KEY=${config_development_PUSHOVER_USER_KEY} \
    -e SLACK_API_TOKEN=${config_development_SLACK_API_TOKEN} \
    -e DEV='True' \
    ${project_name}-dev

echo "Tailing ${project_name} Container Logs"

# If logs go to stdout in container docker logs with work, otherwise you will want to exec into the container and tail your specific log file
docker logs -f ${project_name}-dev
