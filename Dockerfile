FROM python:3.8.0a3-alpine3.9
# source: https://github.com/docker-library/python/blob/4e284ff8ad5458518743188afa3e64d83ea9986c/3.8-rc/alpine3.9/Dockerfile

COPY requirements.txt /tmp/requirements.txt

# Update base alpine packages and install python dependencies from requirements.txt
RUN apk update && \
    apk upgrade && \
    python3 -m pip --no-cache-dir install -r \
    /tmp/requirements.txt && \
    rm -rf /tmp/requirements.txt && \
    rm -rf /tmp/build && \
    rm -rf /var/cache/apk/*

COPY fs-overlay/ /

ENTRYPOINT ["/opt/evga_watch/sbin/init"]
